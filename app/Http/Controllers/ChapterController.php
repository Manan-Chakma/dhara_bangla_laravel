<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ChapterController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function store(Request $request) {
        try {
            $zero = 0;
            request()->validate([
                'chap_dir_id' => "required|gte:$zero",
                'chap_name_input' => "required",
                'chap_thumbnail'  => 'required|mimes:jpeg,bmp,png,jpg',
            ]);
            $path = Storage::putFile('chapter_thumbnails', $request->file('chap_thumbnail'));


            DB::connection('mysql2')
                ->table('directories')
                ->where('id', $request->input('chap_dir_id'))
                ->update(['chapter_exists' => 'Yes']);

            DB::connection('mysql2')
                ->table('chapters')
                ->insert([
                    'chapter_name' => $request->input('chap_name_input'),
                    'chapter_thumbnail' => $path,
                    'directory_id' => $request->input('chap_dir_id')
                ]);

        } catch (QueryException $e) {
            return response()->json(['message' => 'query exception', 'status' => 400]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);
        }
        return response()->json(['message' => 'Successful', 'status' => 200]);
    }

    public function update(Request $request) {
        try {
            $zero = 0;
            $this->validate($request, [
                'update_chap_select' => "required|gte:$zero",
                'update_chapter_name_input' => 'required',
                'update_chap_thumbnail' => 'required|mimes:jpeg,bmp,png,jpg'
            ]);
            $path = Storage::putFile('chapter_thumbnails', $request->file('update_chap_thumbnail'));
            DB::connection('mysql2')
                ->table('chapters')
                ->where('id', '=', $request->input('update_chap_select'))
                ->update([
                    'chapter_name' => $request->input('update_chapter_name_input'),
                    'chapter_thumbnail' => $path,
                ]);
        } catch (QueryException $e) {
            return response()->json(['message' => 'query exception', 'status' => 400]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);
        }
        return response()->json(['message' => 'Successful', 'status' => 200]);
    }


    public function delete(Request $request) {
        $zero = 0;
        try {
            $this->validate($request, [
                'id' => "required:gte:$zero",
            ]);
            DB::connection('mysql2')
                ->table('chapters')
                ->where('id', $request->input('id'))
                ->delete();
        } catch (QueryException $e) {
            return $this->sendResponse('exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }
    public function sendResponse($msg, $status) {
        return response()->json([
            'message' => $msg,
            'status' => $status
        ]);
    }
}
