<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function store(Request $request) {

        $this->validate($request, [
            'gallery_name_input' => 'required',
            'gallery_desc_input' => 'required',
            'dhara_class_select' => 'required',
            'gallery_photo' => 'required|mimes:jpeg,bmp,png,jpg'
        ]);
        try {
            $path = Storage::putFile('gallery_photos', $request->file('gallery_photo'));
            DB::connection('mysql2')
                ->table('gallery')
                ->insert([
                    'gallery_name' => $request->input('gallery_name_input'),
                    'gallery_thumbnail' => $path,
                    'gallery_description' => $request->input('gallery_desc_input'),
                    'class_id' => $request->input('dhara_class_select'),
                ]);

        } catch (QueryException $e) {
            return response()->json(['message' => 'query exception', 'status' => 400]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);
        }
        return response()->json(['message' => 'Successful', 'status' => 200]);
    }
}
