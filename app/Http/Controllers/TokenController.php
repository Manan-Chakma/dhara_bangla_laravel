<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TokenController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function sendResponse($msg, $status) {
        return response()->json([
            'message' => $msg,
            'status' => $status
        ]);
    }

    public function index() {
        $tokens = DB::connection('mysql2')->table('tokens')
            ->select('id', 'token')
            ->whereNull('user_phone')
            ->where('is_used', '0')
            ->get();
        if (is_null($tokens)) {
            $tokens = collect([]);
        }

        return view('tokens', [
            'tokens' => $tokens,
            'available_tokens' => $tokens->count(),
        ]);
    }

    public function store(Request $request) {

        try {
            $this->validate($request, [
                'num_of_tokens' => 'required'
            ]);
            $num = $request->input('num_of_tokens');
            if ($num == 'selected') {
                return redirect()->back();
            }
            $num = intval($num);
            $result_arr = [];
            for ($i = 0; $i < $num; $i++) {
                array_push($result_arr, array(
                    'token' => rand(100000, 999999), 'created_at' => date("Y-m-d"), 'updated_at' => date("Y-m-d")
                ));
            }
            DB::connection('mysql2')
                ->table('tokens')
                ->insert($result_arr);


        } catch (QueryException $e) {
            return view('404');
        } catch (\Exception $e) {
            return view('404');
        }
        return redirect()->back();
    }


    public function show($id) {
        //
    }


    public function update(Request $request) {
        try {

            $this->validate($request, [
                'phone_no' => 'required',
                'token_id' => 'required'
            ]);
            DB::connection('mysql2')
                ->table('tokens')
                ->where('id', $request->input('token_id'))
                ->update([
                    'user_phone' => $request->input('phone_no'),
                    'updated_at' => date("Y-m-d")
                ]);

        } catch (QueryException $e) {
            return $this->sendResponse('query exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('updated', 200);
    }


}
