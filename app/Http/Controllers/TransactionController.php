<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function update(Request $request){
        try {
            $this->validate($request, [
                "id" => 'required',
                "isChecked" => 'required'
            ]);

            $userId = $request->input('id');
            $isChecked = $request->input('isChecked');
            if($isChecked == 'true'){
                // extend 6 month
                $date = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                $sDate = $date->format('Y-m-d h:i:s');

                $date = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                $date->modify('+6 month');
                $eDate = $date->format('Y-m-d h:i:s');

                DB::connection('mysql2')
                    ->table('users')
                    ->where('id', '=', $userId)
                    ->update([
                        'access_startdate' => $sDate,
                        'access_enddate' => $eDate
                    ]);

            } else {
                // update the access end date

                $date = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                $date->modify('-2 day');
                $sDate = $date->format('Y-m-d h:i:s');

                $date = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                $date->modify('-1 day');
                $eDate = $date->format('Y-m-d h:i:s');

                DB::connection('mysql2')
                    ->table('users')
                    ->where('id', '=', $userId)
                    ->update([
                        'access_startdate' => $sDate,
                        'access_enddate' => $eDate
                    ]);
            }
        }catch (\Exception $e){
            return response()->json(["status" => 404]);
        }

        return response()->json(["status" => 200]);
    }
    public function store(Request $request) {
        try {
            $this->validate($request, [
                'userId' => "required",
                'transactionNumber' => 'required',
                'accessStartDate' => "required",
                'accessEndDate' => 'required'
            ]);
//            return response()->json(["req" => $request->keys(),"status" => 200]);
            $sDate = $request->input('accessStartDate');
            $eDate = $request->input('accessEndDate');
            $userId = $request->input('userId');
            $transNum = strval($request->input('transactionNumber'));
            DB::transaction(function () use ($eDate, $sDate, $transNum, $userId) {
                DB::connection('mysql2')
                    ->table('transactions')
                    ->insert(
                        [
                            'user_id' => $userId,
                            'trx_num' => $transNum,
                            'access_startdate' => $sDate,
                            'access_enddate' => $eDate,
                            'created_at' => Carbon::now()
                        ]
                    );
                DB::connection('mysql2')
                    ->table('users')
                    ->where('id', '=', $userId)
                    ->update([
                        'access_startdate' => $sDate,
                        'access_enddate' => $eDate
                    ]);
            });


        } catch (QueryException $e) {
            return response()->json(["status" => 400]);
        } catch (\Exception $e) {
            return response()->json(["status" => 404]);
        }
        return response()->json(["status" => 200]);
    }

    public function getPhoneSuggestion() {
        try {
            $nums = DB::connection('mysql2')
                ->table('users')
                ->select('phone')
                ->get();
        } catch (QueryException $e) {
            $nums = collect([]);
            return response()->json(["p_numbers" => $nums, "status" => 400]);
        } catch (\Exception $e) {
            $nums = collect([]);
            return response()->json(["p_numbers" => $nums, "status" => 404]);
        }
        return response()->json(["p_numbers" => $nums, "status" => 200]);
    }
}
