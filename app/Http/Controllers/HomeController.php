<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use function PHPUnit\Framework\isNull;

class HomeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }
    public function userControlCall() {
        $query = DB::connection('mysql2')
            ->table('users')
            ->select('id','name', 'phone', 'class_id', 'access_startdate', 'access_enddate');

        return DataTables::of($query)->make(true);
    }

    public function gotoTransaction() {
        $phoneNumbers = collect([]);
        try {
            $phoneNumbers = DB::connection('mysql2')
                ->table('users')
                ->select('id','phone')
                ->get();
        } catch (QueryException $e) {
            $phoneNumbers = collect([]);
        } catch (\Exception $e) {
            $phoneNumbers = collect([]);
        }
        return view('transactions',
            [
                'phone_numbers' => $phoneNumbers,
                'total_phone_numbers' => $phoneNumbers->count()
            ]
        );
    }

    private function getDir($class_id) {
        return DB::connection('mysql2')->table('directories')->select('id', 'directory_name')
            ->where('class_id', '=', $class_id)
            ->where('parent', '!=', 0)
            ->get();

    }

    private function getChapter($class_id) {
        try {
            return DB::connection('mysql2')->table('chapters')
                ->select('id', 'chapter_name')
                ->whereIn("directory_id", function ($query) use ($class_id) {
                    $query->select('id')
                        ->from('directories')
                        ->where('class_id', '=', $class_id)
                        ->where('chapter_exists', '=', "Yes");
                })->get();
        } catch (QueryException $exception) {
            return collect([]);
        } catch (\Exception $e) {
            return collect([]);
        }
    }

    public function selectChanged(Request $request) {
        $chapterId = $request->input('id');
        return response()->json(["lecs" => $this->getLectureBy($chapterId)]);
    }

    private function getLectureBy($chapterId) {
        return DB::connection('mysql2')->table('lectures')->select('id', 'lecture_name', 'lecture_description')
            ->where('chapter_id', '=', $chapterId)
            ->get();
    }

    public function index(Request $request) {
        $classes = DB::connection('mysql2')->table('class')->select('id', 'class_name')->get();
        if ($request->has("level") && count($request->keys()) == 1) {
            $level = $request->input("level");
            if ($level == "ssc") {
                $dirs = $this->getDir(1);
                $chaps = $this->getChapter(1);
                $directories = $dirs;
                $chapters = $chaps;
                $class_id = 1;
//                return response()->json(["class_id" => 1, "directories" => $directories, "chapters" => $chapters]);
            } elseif ($level == "hsc") {
                $dirs = $this->getDir(2);
                $chaps = $this->getChapter(2);
                $directories = $dirs;
                $chapters = $chaps;
                $class_id = 2;

//                return response()->json(["class_id" => 2, "directories" => $directories, "chapters" => $chapters]);
            } elseif ($level == "bcs") {
                $dirs = $this->getDir(3);
                $chaps = $this->getChapter(3);
                $directories = $dirs;
                $chapters = $chaps;
                $class_id = 3;
//                return response()->json(["class_id" => 3, "directories" => $directories, "chapters" => $chapters]);
            } else {
                $directories = collect([]);
                $chapters = collect([]);
                $class_id = -1;
            }
        } else {
            return redirect()->route("welcome");
        }
        $lectures = collect([]);
        if ($chapters->count() > 0) {
            $firstChapterId = $chapters->first()->id;
            $lectures = $this->getLectureBy($firstChapterId);
        }
        return view('home', [
            'dir' => $directories,
            'dir_count' => $directories->count(),
            'chapters' => $chapters,
            'chapters_count' => $chapters->count(),
            'lectures' => $lectures,
            'lectures_count' => $lectures->count(),
            'class_id' => $class_id,
            'dhara_class' => $classes,
        ]);

    }


    public function gotoUpdate(Request $request) {

        if ($request->has("level")) {
            $level = $request->input("level");
            if ($level == "ssc") {
                $chaps = $this->getChapter(1);
                $chapters = $chaps;
                $class_id = 1;
            } elseif ($level == "hsc") {
                $chaps = $this->getChapter(2);
                $chapters = $chaps;
                $class_id = 2;

//                return response()->json(["class_id" => 2, "directories" => $directories, "chapters" => $chapters]);
            } elseif ($level == "bcs") {
                $chaps = $this->getChapter(3);
                $chapters = $chaps;
                $class_id = 3;
//                return response()->json(["class_id" => 3, "directories" => $directories, "chapters" => $chapters]);
            } else {
                $chapters = collect([]);
                $class_id = -1;
            }
        } else {
            return redirect()->route("welcome");
        }

        $lectures = DB::connection('mysql2')
            ->table('lectures')->select('id', 'lecture_name', 'lecture_description')
            ->get();
        return view('update', [
            'chapters' => $chapters,
            'chapters_count' => $chapters->count(),
            'lectures' => $lectures,
            'lectures_count' => $lectures->count(),
            'class_id' => $class_id,
        ]);

    }

    private function getSSCLectures($chapters) {
        $lectures = collect([]);
        if ($chapters->count() > 0) {
            $firstChapterId = $chapters->first()->id;
            $lectures = $this->getLectureBy($firstChapterId);
        }
        return $lectures;
    }

    private function returnLectureInsert($chapters, $lectures, $class_id) {
        return view('lecture_insert', [
            'chapters' => $chapters,
            'chapters_count' => $chapters->count(),
            'lectures' => $lectures,
            'lectures_count' => $lectures->count(),
            'class_id' => $class_id,
        ]);
    }
}
