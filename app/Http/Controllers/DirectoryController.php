<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DirectoryController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }
    public function sendResponse($msg, $status) {
        return response()->json([
            'message' => $msg,
            'status' => $status
        ]);
    }

    public function store(Request $request) {
        try {
            $this->validate($request, [
                'directory_name' => 'required',
                'class_id' => 'required',
                'parent_id' => 'required',
            ]);
            DB::connection('mysql2')->table('directories')->insert([
                'class_id' => $request->input('class_id'),
                'directory_name' => $request->input('directory_name'),
                'parent' => $request->input('parent_id'),
                'chapter_exists' => "No",
            ]);
        } catch (QueryException $e) {
            return $this->sendResponse('query exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }





    public function update(Request $request) {


        try {
            $this->validate($request, [
                'id' => 'required',
                'directory_name' => 'required',
            ]);
            DB::connection('mysql2')
                ->table('directories')
                ->where('id', $request->input('id'))
                ->update([
                    'directory_name' => $request->input('directory_name'),
                ]);
        } catch (QueryException $e) {
            return $this->sendResponse('query exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }


    public function delete(Request $request) {
        try {
            $this->validate($request, [
                'id' => 'required',
            ]);
            DB::connection('mysql2')
                ->table('directories')
                ->where('id', $request->input('id'))
                ->delete();
        } catch (QueryException $e) {
            return $this->sendResponse('exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }
}
