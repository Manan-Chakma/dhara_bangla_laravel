<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Type;

class LectureController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        try {
            $this->validate($request, [
                'id' => 'required',
            ]);
            $lec = DB::connection('mysql2')
                ->table('lectures')
                ->select('lecture_description')
                ->where('id', '=', $request->input('id'))
                ->get();

            return response()->json(['lec_description' => $lec->first()->lecture_description, 'message' => 'Successful', 'status' => 200]);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);

        }
    }

    public function sendResponse($msg, $status) {
        return response()->json([
            'message' => $msg,
            'status' => $status
        ]);
    }

    // text insert
    public function store(Request $request) {
        try {
            $this->validate($request, [
                'lecture_name' => 'required',
                'lecture_desc' => 'required',
                'chapter_id' => 'required',
                'lecture_thumbnail' => 'required|mimes:jpeg,bmp,png,jpg',
            ]);
            $path = Storage::putFile('lecture_thumbnails', $request->file('lecture_thumbnail'));
            DB::connection('mysql2')->table('lectures')->insert([
                'lecture_name' => $request->input('lecture_name'),
                'lecture_description' => $request->input('lecture_desc'),
                'chapter_id' => $request->input('chapter_id'),
                'lecture_thumbnail' => $path
            ]);
        } catch (QueryException $e) {
            return response()->json(['message' => 'query exception', 'status' => 400]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);
        }
        return response()->json(['message' => 'Successful', 'status' => 200]);
    }

    private function isAlreadyEncrypted($lectureId) {
        try {
            $type = DB::connection('mysql2')
                ->table('lectures')
                ->select('lecture_encrypted')
                ->where('id', $lectureId)
                ->get();
        } catch (QueryException $e) {
            return "ERR";
        }
        return $type->first()->lecture_encrypted;
    }

    private function encrpt($file) {
        $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$#';
        $f_shuffled = str_shuffle($str);
        $e_shuffled = str_shuffle($str);
        $video_data = base64_encode(file_get_contents($file));
        $video_data = $f_shuffled.$video_data.$e_shuffled;
        $date = new DateTime();
        $time = $date->getTimestamp();
        $file_name = "lecture_videos/".uniqid(time()).strval($time).".dharabangla";
        Storage::disk('local')->put($file_name, $video_data);
        return $file_name;
    }

    private function isInsertingSameEncryptedData($encVidType, $type) {
        return $encVidType == $type;
    }

    public function update(Request $request) {
        $zero = 0;
        try {
            $this->validate($request, [
                'id' => "required|gte:$zero",
                'lec_name' => 'required',
                'lec_desc' => 'required',
            ]);
            DB::connection('mysql2')
                ->table('lectures')
                ->where('id', $request->input('id'))
                ->update([
                    'lecture_name' => $request->input('lec_name'),
                    'lecture_description' => $request->input('lec_desc')
                ]);
        } catch (QueryException $e) {
            return $this->sendResponse('exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }


    public function delete(Request $request) {
        $zero = 0;
        try {
            $this->validate($request, [
                'id' => "required|gte:$zero",
            ]);
            DB::connection('mysql2')
                ->table('lectures')
                ->where('id', $request->input('id'))
                ->delete();
        } catch (QueryException $e) {
            return $this->sendResponse('exception', 404);
        } catch (\Exception $e) {
            return $this->sendResponse('exception', 404);
        }
        return $this->sendResponse('successful', 200);
    }


    public function upload(Request $request) {
        try {
            $zero = 0;
            $this->validate($request, [
                'stream_lecture_select' => "required|gte:$zero",
                'stream_chapter_select' => "required|gte:$zero",
                'stream_type_select' => 'required',
                'lecture_video' => 'required|mimes:mp4,mpeg,flv,avi'
            ]);
            $type = $request->input('stream_type_select');
            $selLecId = $request->input('stream_lecture_select');
            $selChapId = $request->input('stream_chapter_select');
            $vidFile = $request->file('lecture_video');
            $encVidType = $this->isAlreadyEncrypted($selLecId);
            if ($encVidType == 'None') {
                $file_name = $this->encrpt($request->file('lecture_video'));
                $path = Storage::putFile('lecture_videos', $vidFile);
                DB::transaction(function () use ($file_name, $type, $path, $selLecId, $selChapId) {

                    DB::connection('mysql2')
                        ->table('lectures')
                        ->where('id', '=', $selLecId)
                        ->update([
                            'lecture_encrypted_url' => $file_name,
                            'lecture_encrypted' => $type,
                        ]);
                    DB::connection('mysql2')
                        ->table('streams')
                        ->insert([
                            'lecture_id' => $selLecId,
                            'stream_url' => $path,
                            'stream_type' => $type,
                            'chapter_id' => $selChapId
                        ]);
                });
            } elseif ($encVidType == '480P' || $encVidType == '720P' || $encVidType == '1080P') {
                // if trying to insert already encrypted and uploaded data that is updating
                if ($this->isInsertingSameEncryptedData($encVidType, $type)) {
                    $this->processForAlreadyEncData($vidFile, $type, $selLecId, $selChapId);
                } else {
                    $this->processForNonEncryptedData($vidFile, $type, $selLecId, $selChapId);
                }
            } else {
                return response()->json(['message' => 'Process Failed', 'status' => 404]);
            }
        } catch (QueryException $e) {
            return response()->json(['message' => 'query exception', 'status' => 400]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to upload data', 'status' => 404]);
        }
        return response()->json(['message' => 'Successful', 'status' => 200]);
    }

    private function processForAlreadyEncData($vidFile, $type, $lecId, $chapId) {
        $file_name = $this->encrpt($vidFile);
        $path = Storage::putFile('lecture_videos', $vidFile);
        DB::transaction(function () use ($file_name, $type, $path, $lecId, $chapId) {
            DB::connection('mysql2')
                ->table('lectures')
                ->where('id', '=', $lecId)
                ->update([
                    'lecture_encrypted_url' => $file_name,
                ]);
            DB::connection('mysql2')
                ->table('streams')
                ->where('lecture_id', '=', $lecId)
                ->where('chapter_id', '=', $chapId)
                ->where('stream_type', '=', $type)
                ->update([
                    'stream_url' => $path,
                ]);
        });
    }

//lecture_videos/1612374260601ae0f4ed5821612374260.dharabangla
    private function processForNonEncryptedData($vidFile, $type, $selLecId, $selChapId) {
        $path = Storage::putFile('lecture_videos', $vidFile);
        DB::connection('mysql2')
            ->table('streams')
            ->updateOrInsert([
                'lecture_id' => $selLecId,
                'stream_url' => $path,
                'stream_type' => $type,
                'chapter_id' => $selChapId
            ]);
    }
}
