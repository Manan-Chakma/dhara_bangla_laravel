<?php

use App\Http\Controllers\ChapterController;
use App\Http\Controllers\DirectoryController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LectureController;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/updates', [HomeController::class, 'gotoUpdate'])->name('updates');
Route::get('/transactions', [HomeController::class, 'gotoTransaction'])->name('transactions');
Route::get('/tokens', [TokenController::class, 'index'])->name('tokens');

Route::post('/transactions/store', [TransactionController::class, 'store'])->name('transaction_insert');
Route::post('/transactions/switch', [TransactionController::class, 'update'])->name('transaction_switch');


Route::get('/user_control_call', [HomeController::class, 'userControlCall'])->name('user_control_call');



//Route::post('/directory', [DirectoryController::class, 'store'])->name('directory');
Route::post('/chapter', [ChapterController::class, 'store'])->name('chapter');
Route::get('/get_lecture', [LectureController::class, 'index'])->name('get_lecture');

Route::post('/lecture', [LectureController::class, 'store'])->name('lecture');
Route::post('/lecture_upload', [LectureController::class, 'upload'])->name('lec_video_upload');
Route::post('/gallery', [GalleryController::class, 'store'])->name('gallery');


//Route::post('/update_directory', [DirectoryController::class, 'update'])->name('update_directory');
Route::post('/update_chapter', [ChapterController::class, 'update'])->name('update_chapter');
Route::post('/update_lecture', [LectureController::class, 'update'])->name('update_lecture');

Route::post('/delete_directory', [DirectoryController::class, 'delete'])->name('delete_directory');
Route::post('/delete_chapter', [ChapterController::class, 'delete'])->name('delete_chapter');
Route::post('/delete_lecture', [LectureController::class, 'delete'])->name('delete_lecture');


Route::post('/token_insert', [TokenController::class, 'store'])->name('insert_token');
Route::post('/token_update', [TokenController::class, 'update'])->name('update_token');


Route::post('/chapter_change', [HomeController::class, 'selectChanged'])->name('chapter_select_change');



Route::fallback(function () {
    return redirect()->back();
});
