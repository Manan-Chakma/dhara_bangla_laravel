<div class="card">
    <div class="card-header"><b>টোকেন তৈরি করুন</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif


        <form method="post" action="{{route('insert_token')}}">
            @csrf
            <div class="form-row align-items-center">
                <div class="col-sm-3 my-1">
                    <div class="input-group">
                        <select class="custom-select" name="num_of_tokens">
                            <option value="selected" selected>টোকেন সংখ্যা</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div>

                <div class="col-auto my-1">
                    <button id="token_generate_button" type="submit" class="btn btn-primary">তৈরি করুন</button>
                </div>
            </div>
        </form>

    </div>

</div>





<div class="card">
    <div class="card-header"><b>টোকেন নিযুক্ত করুন</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif


            <form>
                <div class="form-row align-items-center">
{{--                    <div class="col-sm-3 my-1">--}}
{{--                        <label class="sr-only" for="inlineFormInputName">Phone</label>--}}
{{--                        <input type="text" class="form-control" id="token_phone_input" placeholder="017********">--}}
{{--                    </div>--}}


                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <table id = "token_table">
                            <thead>
                                <tr>
                                    <th>Available Tokens</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($available_tokens <= 0)
                                <tr>
                                    <td>None</td>
                                </tr>
                            @else
                                @foreach($tokens as $token)
                                    <tr>
                                        <td>{{$token->token}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
{{--                        <div class="input-group">--}}
{{--                            <select id="token_select" class="custom-select">--}}
{{--                                <option value="selected" selected>Select Token</option>--}}

{{--                                @if($available_tokens <= 0)--}}
{{--                                    <option value="-1">None</option>--}}
{{--                                @else--}}
{{--                                    @foreach($tokens as $token)--}}
{{--                                        <option value="{{$token->id}}">{{$token->token}}</option>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}

{{--                            </select>--}}
{{--                        </div>--}}
                    </div>


{{--                    <div class="col-auto my-1">--}}
{{--                        <button id="token_submit" type="submit" class="btn btn-primary">টোকেন প্রদান করুন</button>--}}
{{--                    </div>--}}
                </div>
            </form>
    </div>

</div>




