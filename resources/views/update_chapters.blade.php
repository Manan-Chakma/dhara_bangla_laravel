<div class="card">
    <div class="card-header"><b>অধ্যায় আপডেট করুন</b> </div>

    <div class="card-body">

        <form id="chapter_update_form" enctype="multipart/form-data" name="chapter_update_form">
            @csrf

            <div class="form-group">
                <label for="lec_chap">অধ্যায়ের নাম</label>

                <select class="form-control" name="update_chap_select">
                    @if($chapters_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($chapters as $chapter)
                            <option value="{{$chapter->id}}">{{$chapter->chapter_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="update_chapter_name">অধ্যায়ের নতুন নাম</label>
                <input type="text" class="form-control" name="update_chapter_name_input">
            </div>

            <label></label>
            <input type="file" name="update_chap_thumbnail" required />
            <input type="submit" class="btn btn-primary" value="আপডেট করুন" />
        </form>



    </div>

</div>



