<div class="card">
    <div class="card-header"><b>গ্যালারী ছবি সংরক্ষণ করুন</b></div>

    <div class="card-body">

        <form id="gallery_insert" name="gallery_insert" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="gallery">ছবির নাম</label>
                <input type="text" name="gallery_name_input" class="form-control" id="gallery_name" aria-describedby="emailHelp" placeholder="">

            </div>

            <div class="form-group">
                <label for="gallery_desc">ছবির বর্ণনা</label>
                <textarea class="form-control" name="gallery_desc_input" id="gallery_desc_id" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="dhara_class">শ্রেণী নির্বাচন করুন</label>
                <select class="form-control" name="dhara_class_select" id="dhara_class_select">
                    @foreach($dhara_class as $c)
                        <option value="{{$c->id}}">{{$c->class_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">

                <label></label>
                <input type="file" name="gallery_photo" required />
                <input type="submit" class="btn btn-primary" value="সংরক্ষণ করুন" />
            </div>

        </form>

    </div>

</div>
