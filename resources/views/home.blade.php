@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
{{--            @include('directories')--}}
            @include('chapters')
            @include('lecture')
            @include('streams')
            @include('gallery')
    </div>
</div>
@endsection
