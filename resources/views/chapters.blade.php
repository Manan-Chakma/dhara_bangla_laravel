<div class="card">
    <div class="card-header"><b>অধ্যায় সংরক্ষণ করুন</b></div>

    <div class="card-body">
        <form id="chap_upload_form" enctype="multipart/form-data" method="post" name="fileinfo">
            @csrf

            <div class="form-group">
                <label for="chap_dir">সাবজেক্ট অথবা সাবজেক্টের অংশের নাম নির্বাচন করুন</label>
                <select class="form-control" name="chap_dir_id" id="chap_dir">
                    @if($dir_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($dir as $directory)
                            <option value="{{$directory->id}}">{{$directory->directory_name}}</option>
                        @endforeach
                    @endif


                </select>
            </div>

            <div class="form-group">
                <label for="chap_name">অধ্যায়ের নাম</label>
                <input type="text" class="form-control" name="chap_name_input" aria-describedby="emailHelp"
                       placeholder="">
                <small id="chap_help" class="form-text text-muted"></small>
            </div>

            <label></label>
            <input type="file" name="chap_thumbnail" required />
            <input type="submit" class="btn btn-primary" value="সংরক্ষণ করুন" />
        </form>
    </div>


</div>
