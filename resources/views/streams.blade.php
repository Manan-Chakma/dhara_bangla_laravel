<div class="card">
    <div class="card-header"> <b>লেকচারের ভিডিও সংরক্ষণ করুন</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form id="lec_video" name="lec_video_data" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="stream_chapter">অধ্যায় নির্বাচন করুন</label>
                <select class="form-control" id="stream_chapter_select" name="stream_chapter_select">

                    @if($chapters_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($chapters as $chapter)
                            <option value="{{$chapter->id}}">{{$chapter->chapter_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="stream_lecture">লেকচারের নির্বাচন করুন</label>
                <select class="form-control" id = "stream_lecture_select" name="stream_lecture_select">
                    @if($lectures_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($lectures as $lecture)
                            <option value="{{$lecture->id}}">{{$lecture->lecture_name}}}</option>
                        @endforeach
                    @endif

                </select>
            </div>


            <div class="form-group">
                <label for="stream_type">ভিডিও মাত্রা</label>
                <select class="form-control" name="stream_type_select">
                    <option>480P</option>
                    <option>720P</option>
                    <option>1080P</option>
                </select>
            </div>

            <div class="form-group">
                <input type="file" name="lecture_video" required />
                <input type="submit" class="btn btn-primary" value="সংরক্ষণ করুন" />

            </div>

        </form>

    </div>

</div>
