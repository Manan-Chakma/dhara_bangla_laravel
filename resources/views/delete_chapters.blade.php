<div class="card">
    <div class="card-header">অধ্যায় ডিলিট করুন</div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form id="delete_chap_form" name="delete_chap_form">
            @csrf
            <div class="form-group">
                <label for="lec_chap">অধ্যায়ের নাম</label>

                <select class="form-control" id="delete_chap_select">
                    @if($chapters_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($chapters as $chapter)
                            <option value="{{$chapter->id}}">{{$chapter->chapter_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>


            <button id="delete-chapter-button" type="submit" class="btn btn-danger">ডিলিট করুন</button>

        </form>

    </div>

</div>



