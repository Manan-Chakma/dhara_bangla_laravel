<div class="card">
    <div class="card-header"><b>User Control</b></div>

    <div class="card-body">

        <table class="table table-bordered" id="user_control">
            <thead>
            <th>Id</th>
            <th>User Name</th>
            <th>Phone</th>
            <th>Class ID</th>
            <th>Access Start Date</th>
            <th>Access End Date</th>
            <th>Controller</th>
{{--            <th>Email</th>--}}
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>
