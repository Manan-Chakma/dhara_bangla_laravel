<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>এডমিন প্যানেল</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    {{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />--}}
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        #switch_input:checked + .slider {
            background-color: #2196F3;
        }

        #switch_input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        #switch_input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                ধারা বাংলা
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>

            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">লগইন</a>
                            </li>
                        @endif

                        {{--                            @if (Route::has('register'))--}}
                        {{--                                <li class="nav-item">--}}
                        {{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        {{--                                </li>--}}
                        {{--                            @endif--}}
                    @else
                        @auth
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    সংরক্ষণ করুন
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/home?level=ssc')}}" type="button"> এস এস সি </a>
                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/home?level=hsc')}}" type="button">এইচ এস সি</a>
                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/home?level=bcs')}}" type="button">বি সি এস</a>

                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    আপডেট করুন
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/updates?level=ssc')}}" type="button"> এস এস সি </a>
                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/updates?level=hsc')}}" type="button">এইচ এস সি</a>
                                    <a id="goto_update" class="dropdown-item btn btn-info"
                                       href="{{url('/updates?level=bcs')}}" type="button">বি সি এস</a>

                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    অন্যান্য
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a id="goto_transactions" class="dropdown-item btn btn-info"
                                       href="{{url('/transactions')}}" type="button">ট্রান্সেকশন</a>
                                    <a id="goto_transactions" class="dropdown-item btn btn-info"
                                       href="{{url('/tokens')}}" type="button">টোকেন</a>

                                </div>

                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        লগআউট
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        @else
                            <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">লগইন করুন</a>
                        @endauth

                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<script>
    function transPhoneNumChange() {
        console.log("changed");
        // console.log(phone_nums);
    }


    function switchCheckOperation(it) {
        var id = $(it).data("id");
        var checked = $(it).is(":checked");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'post',
            url: '{{route('transaction_switch')}}',
            data: {
                id: id,
                isChecked: checked
            },
            success: function (response) {
                location.reload();
                if (response.status === 200) {

                    return alert('Data Updated, Please refresh the page')
                }
                return alert('Something went wrong while inserting data')
            },
            error: function (err) {
                location.reload();
                return alert('Error while uploading data or uploading chapter thumbnail')
            }
        });


    }

    $(document).ready(function () {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#token_table').DataTable();

        $('#user_control').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('user_control_call') }}",
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {
                    "data": "phone",
                    "render": function (data, type, row) {
                        if (data === null) {
                            return "No Phone Number";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "data": "class_id",
                    "render": function (data, type, row) {
                        if (data === 1) {
                            return "SSC";
                        } else if (data === 2) {
                            return "HSC";
                        } else {
                            return "BCS";
                        }
                    }
                },
                {
                    "data": "access_startdate",
                    "render": function (data, type, row) {
                        if (data === null) {
                            return "No Access Date";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "data": "access_enddate",
                    "render": function (data, type, row) {
                        if (data === null) {
                            return "No Access Date";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "render": function (data, type, row) {
                        var sDate = row.access_startdate;

                        var dataId = row.id;
                        var dataAccessStartDate = -1;
                        var dataAccessEndDate = -1;

                        var sDateYear = -1;
                        var sMonth = -1;
                        var sDay = -1;
                        var sHrs = -1;
                        var sMin = -1;
                        var sSec = -1;
                        //
                        if (sDate !== null) {
                            sDateYear = sDate.substring(0, 4);
                            sMonth = sDate.substring(5, 7);
                            sDay = sDate.substring(8, 11);
                            sHrs = sDate.substring(11, 13);
                            sMin = sDate.substring(14, 16);
                            sSec = sDate.substring(17, 19);

                            // sDate = new Date(`${sMonth}/${sDay}/${sDateYear}`);
                            sDate = new Date(
                                parseInt(sDateYear),
                                parseInt(sMonth) - 1,
                                parseInt(sDay),
                                parseInt(sHrs),
                                parseInt(sMin),
                                parseInt(sSec),
                                0
                            );
                            dataAccessStartDate = row.access_startdate;

                        }

                        var eDate = row.access_enddate;
                        //
                        var eDateYear = -1;
                        var eMonth = -1;
                        var eDay = -1;
                        var eHrs = -1;
                        var eMin = -1;
                        var eSec = -1;
                        //
                        if (eDate !== null) {
                            eDateYear = eDate.substring(0, 4);
                            eMonth = eDate.substring(5, 7);
                            eDay = eDate.substring(8, 11);
                            eHrs = eDate.substring(11, 13);
                            eMin = eDate.substring(14, 16);
                            eSec = eDate.substring(17, 19);

                            dataAccessEndDate = row.access_enddate;
                            // eDate = new Date(`${eMonth}/${eDay}/${eDateYear}`);
                            eDate = new Date(parseInt(eDateYear),
                                parseInt(eMonth) - 1,
                                parseInt(eDay),
                                parseInt(eHrs),
                                parseInt(eMin),
                                parseInt(eSec),
                                0
                            );
                        }
                        var unCheckedHtml = `<label class="switch">` +
                            `<input onclick="switchCheckOperation($(this))" data-id="${dataId}" data-adate="${dataAccessStartDate}" data-edate="${dataAccessEndDate}" id = "switch_input" type="checkbox">` +
                            `<span class="slider round"></span>` +
                            `</label>`;
                        //
                        var checkedHtml = `<label class="switch">` +
                            `<input onclick="switchCheckOperation($(this))" data-id="${dataId}" data-adate="${dataAccessStartDate}" data-edate="${dataAccessEndDate}" id = "switch_input" type="checkbox" checked>` +
                            `<span class="slider round"></span>` +
                            `</label>`;
                        //
                        var cDate = new Date();
                        if (sDate === null || eDate === null) {
                            return unCheckedHtml;
                        }
                        if (sDate.getTime() <= cDate.getTime() && cDate.getTime() <= eDate.getTime()) {
                            return checkedHtml;
                        } else if (cDate.getTime() >= sDate.getTime() && cDate.getTime() >= eDate.getTime()) {
                            return unCheckedHtml;
                        } else {
                            return unCheckedHtml;
                        }
                    }
                }
            ]

        });

        // var whichOneChecked = function(){
        //     console.log('called');
        // }
        // $("#switch_input[type=checkbox]").on('click', whichOneChecked);

        var chapterUpdateForm = $("#chapter_update_form");
        var chapterUpdateFormData = document.forms.namedItem("chapter_update_form");

        var transaction_form = $("#transaction_form");
        var trans_id = $("#transaction_id_input");
        var trans_access_from = $("#access_from");
        var trans_access_to = $("#access_to");

        // var srchPhoneNum = $("#token_phone_input");
        var sPhoneNum = $("#search_phone_num");
        var pNumDropDown = $("#phone_num_dropdown");
        var pNumDropDownSelected = $("#phone_num_dropdown option:selected");

        // srchPhoneNum.keyup(function () {
        //     var num = $(this).val();
        //     var mHtml = "";
        //     if(num.length > 3) {
        //         $("#token_phone_filter > option").each(function () {
        //             if($(this).text().includes(num)) {
        //                 mHtml += `<option value=${$(this).text()}>${$(this).text()}</option>`;
        //             }
        //         });
        //         if(mHtml === "") {
        //             $("#token_phone_filter").html("<option value=-1>None</option>")
        //         } else {
        //             $("#token_phone_filter").html(mHtml);
        //         }
        //     }
        // });
        sPhoneNum.keyup(function () {
            var num = $(this).val();
            var mHtml = "";
            if (num.length > 3) {
                $("#phone_num_dropdown > option").each(function () {
                    // console.log($(this).text());
                    if ($(this).text().includes(num)) {
                        mHtml += `<option value=${$(this).val()}>${$(this).text()}</option>`;
                    }
                });
                if (mHtml === "") {
                    pNumDropDown.html("<option value=-1>None</option>")
                } else {
                    pNumDropDown.html(mHtml);
                }
            }
        });


        chapterUpdateForm.submit(function (ev) {
            ev.preventDefault();
            var fData = new FormData(chapterUpdateFormData);

            $.ajax({
                method: 'post',
                url: '{{route('update_chapter')}}',
                data: fData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data Updated, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();

                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            var uploadPercent = e.loaded / e.total;
                            console.log(uploadPercent);
                            // uploadPercent = (uploadPercent * 100);
                            //
                            // $('.progress-bar').text(uploadPercent + '%');
                            // $('.progress-bar').width(uploadPercent + '%');
                            //
                            // if(uploadPercent == 100){
                            //     $('.progress-bar').text('Completed');
                            //     $('#completed').text('File Uploaded');
                            // }
                        }
                    }, false);

                    return xhr;
                },

                error: function (err) {
                    return alert('Error while uploading data or uploading chapter thumbnail')
                }
            });

        });

        transaction_form.submit(function (ev) {
            ev.preventDefault();
            if (
                trans_id.val().length <= 0 ||
                trans_access_from.val().length < 10 ||
                trans_access_to.val().length < 10
            ) {
                return alert("Field values are missing");
            }
            var sDate = trans_access_from.val();
            var eDate = trans_access_to.val();
            // sDate = sDate[8] + sDate[9] + "/" + sDate[5] + sDate[6] + "/" + sDate[0] + sDate[1] + sDate[2] + sDate[3];
            // eDate = eDate[8] + eDate[9] + "/" + eDate[5] + eDate[6] + "/" + eDate[0] + eDate[1] + eDate[2] + eDate[3];

            var uId = pNumDropDownSelected.val();
            var transactionNumber = trans_id.val()

            $.ajax({
                method: 'post',
                url: '{{route('transaction_insert')}}',
                data: {
                    userId: uId,
                    transactionNumber: transactionNumber,
                    accessStartDate: sDate,
                    accessEndDate: eDate,

                },
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Transaction number with user inserted, Please refresh the page')
                    }
                    console.log(response);
                    return alert('Something went wrong while inserting transaction data')
                },
                error: function (err) {
                    return alert('Error while inserting transaction number')
                }
            });

        })

    });
</script>
<script>

    $(document).ready(function () {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var form = $("#chap_upload_form");
        var streamUploadForm = $("#lec_video");
        var galleryUploadForm = $("#gallery_insert");
        var lectureUploadForm = $("#new_lecture_insert");

        var formData = document.forms.namedItem("fileinfo");
        var streamUploadFormData = document.forms.namedItem("lec_video_data");
        var galleryUploadFormData = document.forms.namedItem("gallery_insert");
        var lectureUploadFormData = document.forms.namedItem("new_lecture_insert");

        lectureUploadForm.submit(function (ev) {
            ev.preventDefault();
            var fData = new FormData(lectureUploadFormData);

            $.ajax({
                method: 'post',
                url: '{{route('lecture')}}',
                data: fData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data inserted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();

                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            var uploadPercent = e.loaded / e.total;
                            console.log(uploadPercent);
                            // uploadPercent = (uploadPercent * 100);
                            //
                            // $('.progress-bar').text(uploadPercent + '%');
                            // $('.progress-bar').width(uploadPercent + '%');
                            //
                            // if(uploadPercent == 100){
                            //     $('.progress-bar').text('Completed');
                            //     $('#completed').text('File Uploaded');
                            // }
                        }
                    }, false);

                    return xhr;
                },

                error: function (err) {
                    return alert('Something went wrong while inserting data or uploading chapter thumbnail')
                }
            });


        });

        form.submit(function (ev) {
            ev.preventDefault();
            var fData = new FormData(formData);

            $.ajax({
                method: 'post',
                url: '{{route('chapter')}}',
                data: fData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data inserted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();

                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            var uploadPercent = e.loaded / e.total;
                            console.log(uploadPercent);
                            // uploadPercent = (uploadPercent * 100);
                            //
                            // $('.progress-bar').text(uploadPercent + '%');
                            // $('.progress-bar').width(uploadPercent + '%');
                            //
                            // if(uploadPercent == 100){
                            //     $('.progress-bar').text('Completed');
                            //     $('#completed').text('File Uploaded');
                            // }
                        }
                    }, false);

                    return xhr;
                },

                error: function (err) {
                    return alert('Something went wrong while inserting data or uploading chapter thumbnail')
                }
            });


        });

        streamUploadForm.submit(function (ev) {
            ev.preventDefault();
            var fData = new FormData(streamUploadFormData);

            $.ajax({
                method: 'post',
                url: '{{route('lec_video_upload')}}',
                data: fData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data inserted, Please refresh the page');
                    }
                    return alert('Something went wrong while inserting data');
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();

                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            var uploadPercent = e.loaded / e.total;
                            // console.log(`${uploadPercent} % uploaded`);
                            // uploadPercent = (uploadPercent * 100);
                            //
                            // $('.progress-bar').text(uploadPercent + '%');
                            // $('.progress-bar').width(uploadPercent + '%');
                            //
                            // if(uploadPercent == 100){
                            //     $('.progress-bar').text('Completed');
                            //     $('#completed').text('File Uploaded');
                            // }
                        }
                    }, false);

                    return xhr;
                },

                error: function (err) {
                    return alert('Something went wrong while inserting data or uploading video')
                }
            });


        });

        galleryUploadForm.submit(function (ev) {
            ev.preventDefault();
            var fData = new FormData(galleryUploadFormData);

            $.ajax({
                method: 'post',
                url: '{{route('gallery')}}',
                data: fData,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data inserted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();

                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            var uploadPercent = e.loaded / e.total;
                            console.log(uploadPercent);
                            // uploadPercent = (uploadPercent * 100);
                            //
                            // $('.progress-bar').text(uploadPercent + '%');
                            // $('.progress-bar').width(uploadPercent + '%');
                            //
                            // if(uploadPercent == 100){
                            //     $('.progress-bar').text('Completed');
                            //     $('#completed').text('File Uploaded');
                            // }
                        }
                    }, false);

                    return xhr;
                },

                error: function (err) {
                    return alert('Something went wrong while inserting data or uploading chapter thumbnail')
                }
            });


        });


        let updatedDirectoryName = $('#updated_dir_name_input');
        let updateDirectoryId = $('#update_dir_id');
        let updateDirectoryButton = $('#update_dir_name_button');
        let streamChapSelect = $("#stream_chapter_select");
        let streamLecSelect = $("#stream_lecture_select");

        streamChapSelect.change(function () {
            let id = streamChapSelect.val();
            $.ajax({
                method: 'post',
                url: '{{route('chapter_select_change')}}',
                data: {
                    id: id
                },
                success: function (response) {
                    var lecturesByChapter = response.lecs;
                    var mHtml = "";
                    if (lecturesByChapter.length <= 0) {
                        mHtml += `<option value="-1">None</option>`;
                    } else {
                        for (var i = 0; i < lecturesByChapter.length; i++) {
                            var lec = lecturesByChapter[i];
                            mHtml += `<option value="` + `${lec.id}` + `">${lec.lecture_name}</option>`;
                        }
                    }
                    streamLecSelect.html(mHtml);

                },

                error: function (err) {
                    var mHtml = "";
                    mHtml += `<option value="-1">None</option>`;
                    streamLecSelect.html(mHtml);
                    return alert('Something went wrong while fetching lectures of respective chapter, please check')
                }
            });

        });

        //let gotoUpdatePage = $('#goto_update');

        {{--updateDirectoryButton.on('click', function (event) {--}}
        {{--    event.preventDefault();--}}
        {{--    console.log(updateDirectoryId.val());--}}
        {{--    console.log(updatedDirectoryName.val());--}}
        {{--    if (updatedDirectoryName.val() < 2) {--}}
        {{--        alert('দৈর্ঘ অবশ্যই একটি অক্ষের চেয়ে বেশি হওয়া আবশ্যক')--}}
        {{--    }--}}
        {{--    let myData = {--}}
        {{--        _token: CSRF_TOKEN,--}}
        {{--        id: updateDirectoryId.val(),--}}
        {{--        directory_name: updatedDirectoryName.val(),--}}
        {{--    }--}}

        {{--    $.ajax({--}}
        {{--        method: 'post',--}}
        {{--        url: '{{route('update_directory')}}',--}}
        {{--        data: myData,--}}
        {{--        success: function (response) {--}}
        {{--            if (response.status === 200) {--}}
        {{--                return alert('Data Updated, Please refresh the page')--}}
        {{--            }--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        },--}}
        {{--        error: function (err) {--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}


        let lectureUpdateButton = $('#update_lecture_button');
        let updateLectureId = $('#update_lecture_id');
        let updateLectureName = $('#update_lecture_name')
        let updateLectureDesc = $('#update_lecture_desc');
        let deleteLecButton = $('#delete_lecture_button');

        updateLectureId.change(function () {
            updateLectureName.val($('#update_lecture_id option:selected').text());
            if(updateLectureId.val() === -1){
                return ;
            }
            let myData = {
                _token: CSRF_TOKEN,
                id: updateLectureId.val(),
            }

            $.ajax({
                method: 'get',
                url: '{{route('get_lecture')}}',
                data: myData,
                success: function (response) {
                    if (response.status === 200) {
                        updateLectureDesc.val(response.lec_description);
                    }
                },
                error: function (err) {
                }
            });

        })

        deleteLecButton.on('click', function (event) {
            event.preventDefault();

            let myData = {
                _token: CSRF_TOKEN,
                id: updateLectureId.val(),
            }

            $.ajax({
                method: 'post',
                url: '{{route('delete_lecture')}}',
                data: myData,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data Deleted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },
                error: function (err) {
                    return alert('Something went wrong while inserting data')
                }
            });

        });

        lectureUpdateButton.on('click', function (event) {
            event.preventDefault();


            if (updateLectureDesc.val() < 2 || updateLectureName.val() < 2) {
                alert('দৈর্ঘ অবশ্যই একটি অক্ষের চেয়ে বেশি হওয়া আবশ্যক')
            }

            let myData = {
                _token: CSRF_TOKEN,
                id: updateLectureId.val(),
                lec_name: updateLectureName.val(),
                lec_desc: updateLectureDesc.val()
            }
            //console.log(myData);

            $.ajax({
                method: 'post',
                url: '{{route('update_lecture')}}',
                data: myData,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data Updated, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },
                error: function (err) {
                    return alert('Something went wrong while inserting data')
                }
            });
        });


        let deleteChapterButton = $('#delete-chapter-button');
        let delChapId = $('#delete_chap_select');

        deleteChapterButton.on('click', function (event) {
            event.preventDefault();

            let myData = {
                _token: CSRF_TOKEN,
                id: delChapId.val(),
            }

            $.ajax({
                method: 'post',
                url: '{{route('delete_chapter')}}',
                data: myData,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data Deleted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },
                error: function (err) {
                    return alert('Something went wrong while inserting data')
                }
            });

        });


        let directoryDeleteButton = $('#directory-delete');

        directoryDeleteButton.on('click', function (event) {
            event.preventDefault();

            let myData = {
                _token: CSRF_TOKEN,
                id: updateDirectoryId.val()
            }

            $.ajax({
                method: 'post',
                url: '{{route('delete_directory')}}',
                data: myData,
                success: function (response) {
                    if (response.status === 200) {
                        return alert('Data Deleted, Please refresh the page')
                    }
                    return alert('Something went wrong while inserting data')
                },
                error: function (err) {
                    return alert('Something went wrong while inserting data')
                }
            });
        });


        // var tokenSubmitButton = $('#token_submit');
        var tokenPhoneNo = $('#token_phone_filter');
        var tokenId = $('#token_select');

        {{--tokenSubmitButton.on('click', function (event) {--}}
        {{--    // event.preventDefault();--}}

        {{--    console.log(tokenId.val());--}}
        {{--    if (tokenId.val() === "selected") {--}}
        {{--        return alert('Select a token');--}}
        {{--    }--}}
        {{--    if (tokenPhoneNo.val().length < 11) {--}}
        {{--        return alert('Phone number must be of 11 character');--}}
        {{--    }--}}
        {{--    $pNo = tokenPhoneNo.val();--}}

        {{--    let myData = {--}}
        {{--        _token: CSRF_TOKEN,--}}
        {{--        phone_no: $pNo,--}}
        {{--        token_id: tokenId.val()--}}
        {{--    }--}}

        {{--    $.ajax({--}}
        {{--        method: 'post',--}}
        {{--        url: '{{route('update_token')}}',--}}
        {{--        data: myData,--}}
        {{--        success: function (response) {--}}
        {{--            if (response.status === 200) {--}}
        {{--                location.reload();--}}
        {{--                return alert('Data Updated, Please refresh the page')--}}
        {{--            }--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        },--}}
        {{--        error: function (err) {--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        }--}}
        {{--    });--}}

        {{--})--}}

    });







    {{--});--}}


</script>


{{--insert script--}}
<script>
    $(document).ready(function () {
        let dirSubmitButton = $('#new_directory_button');
        // let chapSubmitButton = $('#new_chapter_button');
        // let lecSubmitButton = $('#new_lecture_button');
        // let streamSubmitButton = $('#new_stream_button');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        {{--dirSubmitButton.on('click', function (e) {--}}
        {{--    let dirInput = $('#dir_name_input');--}}
        {{--    let classSelect = $('#dir_class_select');--}}
        {{--    let parentSelect = $('#parent_select');--}}

        {{--    e.preventDefault();--}}
        {{--    let dir_name = dirInput.val();--}}
        {{--    if (dir_name.length <= 1) {--}}
        {{--        return alert('Minimum two character required');--}}
        {{--    }--}}
        {{--    let classId = classSelect.val();--}}
        {{--    let parentId = parentSelect.val();--}}

        {{--    //console.log(`dir_name: ${dir_name} classid: ${classId}  chapter: ${hasChapter} parentId: ${parentId}`);--}}

        {{--    let myData = {--}}
        {{--        _token: CSRF_TOKEN,--}}
        {{--        directory_name: dir_name,--}}
        {{--        class_id: classId,--}}
        {{--        parent_id: parentId--}}
        {{--    }--}}

        {{--    $.ajax({--}}
        {{--        method: 'post',--}}
        {{--        url: '{{route('directory')}}',--}}
        {{--        data: myData,--}}
        {{--        success: function (response) {--}}
        {{--            if (response.status === 200) {--}}
        {{--                return alert('Data Inserted, Please refresh the page')--}}
        {{--            }--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        },--}}
        {{--        error: function (err) {--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        }--}}
        {{--    });--}}

        {{--});--}}


        {{--lecSubmitButton.on('click', function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let lecInput = $('#lecture_name_input');--}}
        {{--    let lecDesc = $('#lecture_desc_input');--}}
        {{--    let chapSelected = $('#lec_chap_select');--}}

        {{--    let lecture_name = lecInput.val();--}}
        {{--    let lecture_desc = lecDesc.val();--}}
        {{--    let chapter_id = chapSelected.val();--}}

        {{--    if (lecture_name.length <= 2 || lecture_desc.length <= 5) {--}}
        {{--        return alert('Minimum 2 character for Name and 5 character for Description required')--}}
        {{--    }--}}

        {{--    let myData = {--}}
        {{--        _token: CSRF_TOKEN,--}}
        {{--        lecture_name: lecture_name,--}}
        {{--        lecture_desc: lecture_desc,--}}
        {{--        chapter_id: chapter_id,--}}

        {{--    }--}}
        {{--    // console.log(`lec_name: ${lecture_name} desc: ${lecture_desc}  chapter id: ${chapter_id} `);--}}


        {{--    $.ajax({--}}
        {{--        method: 'post',--}}
        {{--        url: '{{route('lecture')}}',--}}
        {{--        data: myData,--}}
        {{--        success: function (response) {--}}
        {{--            if (response.status === 200) {--}}
        {{--                return alert('Data Inserted, Please refresh the page')--}}
        {{--            }--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        },--}}
        {{--        error: function (err) {--}}
        {{--            return alert('Something went wrong while inserting data')--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}

    });

</script>
{{--<script>--}}
{{--    $(document).ready(function(){--}}

{{--        $('#chapter_upload').ajaxForm({--}}
{{--            beforeSend:function(){--}}
{{--                $('#success').empty();--}}
{{--            },--}}
{{--            uploadProgress:function(event, position, total, percentComplete)--}}
{{--            {--}}
{{--                $('.progress-bar').text(percentComplete + '%');--}}
{{--                $('.progress-bar').css('width', percentComplete + '%');--}}
{{--            },--}}
{{--            success:function(data)--}}
{{--            {--}}
{{--                if(data.errors)--}}
{{--                {--}}
{{--                    $('.progress-bar').text('0%');--}}
{{--                    $('.progress-bar').css('width', '0%');--}}
{{--                    $('#success').html('<span class="text-danger"><b>'+data.errors+'</b></span>');--}}
{{--                }--}}
{{--                if(data.success)--}}
{{--                {--}}
{{--                    $('.progress-bar').text('Uploaded');--}}
{{--                    $('.progress-bar').css('width', '100%');--}}
{{--                    $('#success').html('<span class="text-success"><b>'+data.success+'</b></span><br /><br />');--}}
{{--                    $('#success').append(data.image);--}}
{{--                }--}}
{{--            }--}}
{{--        });--}}

{{--    });--}}
{{--</script>--}}
</body>
</html>
