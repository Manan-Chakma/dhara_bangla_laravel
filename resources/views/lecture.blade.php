<div class="card">
    <div class="card-header"><b>লেকচার সংরক্ষণ করুন</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form id="new_lecture_insert" name="new_lecture_insert" enctype="multipart/form-data">
            <div class="form-group">
                <label for="lec_chap">অধ্যায়ের নাম</label>

                <select class="form-control" id="lec_chap_select" name="chapter_id">
                    @if($chapters_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($chapters as $chapter)
                            <option value="{{$chapter->id}}">{{$chapter->chapter_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="lecture_name">লেকচারের শিরোনাম</label>
                <input type="text" class="form-control" id="lecture_name_input" name="lecture_name">
                <small id="lecHelp" class="form-text text-muted"></small>
            </div>

            <div class="form-group">
                <label for="lecture_description">লেকচারের বর্ণনা</label>
                <textarea class="form-control" id="lecture_desc_input" name="lecture_desc" rows="20"></textarea>
            </div>
            <div class="form-group">

                <label></label>
                <input type="file" name="lecture_thumbnail" required />
                <input type="submit" class="btn btn-primary" value="সংরক্ষণ করুন" />
            </div>
        </form>

    </div>

</div>
