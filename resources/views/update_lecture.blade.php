<div class="card">
    <div class="card-header"><b>লেকচার আপডেট করুন</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form>
            <div class="form-group">
                <label for="lecture_id">লেকচারের নির্বাচন করুন</label>
                <select class="form-control" id="update_lecture_id">

                    @if($lectures_count <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($lectures as $lecture)
                            <option value="{{$lecture->id}}">{{$lecture->lecture_name}}</option>
                        @endforeach
                    @endif

                </select>
            </div>

            <div class="form-group">
                <label for="lecture_name">লেকচারের শিরোনাম</label>
                <input type="text" class="form-control" id="update_lecture_name">
                <small id="lecHelp" class="form-text text-muted"></small>
            </div>

            <div class="form-group">
                <label for="lecture_description">লেকচারের বর্ণনা</label>
                <textarea class="form-control" id="update_lecture_desc" rows="20"></textarea>
            </div>
            <button id="update_lecture_button" type="submit" class="btn btn-primary">আপডেট করুন</button>
            <button id="delete_lecture_button" type="submit" class="btn btn-danger">ডিলিট করুন</button>
        </form>

    </div>

</div>
