<div class="card">
    <div class="card-header"><b>মিসিং ট্রান্সেকশন সংরক্ষন করুন</b></div>

    <div class="card-body">

        <form id="transaction_form" name="transaction_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="gallery">Transaction ID</label>
                <input type="text" name="transaction_id_input" class="form-control" id="transaction_id_input"
                       aria-describedby="emailHelp" placeholder="Transaction ID">

            </div>


            <div class="form-group">
                <label for="gallery_desc">Search Phone Number</label>
                <input id="search_phone_num" class="form-control" type="search">
            </div>


            <div class="form-group">
                <select class="form-control" id="phone_num_dropdown">
                    @if($total_phone_numbers <= 0)
                        <option value="-1">None</option>
                    @else
                        @foreach($phone_numbers as $user)
                            <option value="{{$user->id}}">{{$user->phone}}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="access_from">From</label>
                <input type="date" id="access_from" name="access_from">
                <label for="access_to">To</label>
                <input type="date" id="access_to" name="access_to">
            </div>


            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="সংরক্ষণ করুন"/>
            </div>

        </form>

    </div>

</div>
