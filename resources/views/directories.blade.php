<div class="card">
    <div class="card-header">সাবজেক্ট অথবা সাবজেক্টের অংশ সংরক্ষণ করুন</div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form>
            @csrf
            <div class="form-group">
                <label for="dir_name">সাবজেক্ট অথবা সাবজেক্টের অংশের নাম</label>
                <input type="text" class="form-control" id="dir_name_input">
                <small id="dirHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="dir_class">শ্রেণী নির্বাচন করুন</label>
                <select class="form-control" id="dir_class_select">
                    <option value="1">নবম - দশম</option>
                    <option value="2">একাদশ - দ্বাদশ </option>
                    <option value="3">বিসিএস</option>
                </select>
            </div>
            <div class="form-group">
                <label for="parent_dir">মূল সাবজেক্ট নির্বাচন করুন</label>
                <select class="form-control" id="parent_select">
                    <option value="0">নেই</option>
                    @foreach($dir as $directory)
                        <option value="{{$directory->id}}">{{$directory->directory_name}}</option>
                    @endforeach
                </select>
            </div>
            <button id="new_directory_button" type="submit" class="btn btn-primary">সংরক্ষণ করুন </button>
        </form>

    </div>


</div>


