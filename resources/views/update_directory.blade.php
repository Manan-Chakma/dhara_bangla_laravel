<div class="card">
    <div class="card-header">সাবজেক্ট অথবা সাবজেক্টের অংশ আপডেট করুন</div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form>
            @csrf
            <div class="form-group">
                <label for="update_dir_name">সাবজেক্টের নতুন নাম</label>
                <input type="text" class="form-control" id="updated_dir_name_input">
            </div>
            <div class="form-group">
                <label for="update_dir_selection">সাবজেক্ট নির্বাচন করুন</label>
                <select class="form-control" id="update_dir_id">
                    @foreach($dir as $directory)
                        <option value="{{$directory->id}}">{{$directory->directory_name}}</option>
                    @endforeach
                </select>
            </div>
            <button id="update_dir_name_button" type="submit" class="btn btn-primary">আপডেট করুন</button>
            <button id="directory-delete" type="submit" class="btn btn-danger">ডিলিট করুন</button>
        </form>

    </div>

</div>



