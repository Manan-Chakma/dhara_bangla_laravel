@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
{{--                @include('update_directory')--}}
                @include('update_chapters')
                @include('delete_chapters')
                @include('update_lecture')
{{--                @include('generate_tokens')--}}
{{--                @include('gallery')--}}
            </div>
        </div>
@endsection
